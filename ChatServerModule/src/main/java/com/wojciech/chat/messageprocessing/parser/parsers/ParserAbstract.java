package com.wojciech.chat.messageprocessing.parser.parsers;

import java.util.List;

import com.wojciech.chat.messageprocessing.MessageType;
import com.wojciech.chat.messageprocessing.dataobjects.MessageFromUserToUserDataContainer;
import com.wojciech.chat.messageprocessing.dataobjects.UserInfo;
import com.wojciech.chat.messageprocessing.parser.MessageWithType;


public abstract class ParserAbstract  {

	public MessageWithType<?> retrieveParametrizedMessageFromInput(String inputString)
	{
		inputString = removePrefixFromMsg(inputString);
		return retrieveDataValueFromInputByMessageType(retrieveMessageTypeFromInput(inputString), inputString);
	}
	
	
	protected abstract String removePrefixFromMsg(String inputString);

	protected abstract MessageType retrieveMessageTypeFromInput(String inputString);
	

	protected MessageWithType<?> retrieveDataValueFromInputByMessageType(MessageType type, String inputString)
	{
		switch (type)
		{
			case Init:
				return new MessageWithType<String>(type, retrieveUserName(inputString));
			case Message:
				MessageFromUserToUserDataContainer messageFromUserToUserDataContainer = retrieveMessageFromUserToUserObjectFromInput(inputString);
				return new MessageWithType<MessageFromUserToUserDataContainer>(type, messageFromUserToUserDataContainer);				
			case GetUsers:
				List<UserInfo> userList = retrieveListOfUsersFromInput(inputString);
				return new MessageWithType<List<UserInfo>>(type,userList);
			case EndConnection:
				return new MessageWithType<String>(type, "");
		default:
			break;
		}
		return null;	
	}
	
	protected abstract String retrieveUserName(String inputString);
	
	protected abstract List<UserInfo> retrieveListOfUsersFromInput(String inputString);
	
	protected abstract MessageFromUserToUserDataContainer retrieveMessageFromUserToUserObjectFromInput(String string);

	@SuppressWarnings("unchecked")
	public String parseMessageToString(MessageWithType<?> messageToParse) 
	{
		String msgInStringFormat = addProtocolPrefix();
		switch (messageToParse.getMessageType())
		{
			case Init:
				msgInStringFormat += parseToStringMessageWithTypeAndString((MessageWithType<String>)messageToParse);
				break;
			case Message:
				msgInStringFormat += parseToStringMessageUserToUser((MessageWithType<MessageFromUserToUserDataContainer>) messageToParse);
				break;
			case GetUsers:
				msgInStringFormat += parseToStringUserList((MessageWithType<List<UserInfo>>) messageToParse);
				break;
			case EndConnection:
				msgInStringFormat += parseToStringMessageWithTypeAndString((MessageWithType<String>) messageToParse);
				break;
			default:
				return null;
		}
		return msgInStringFormat;		
	}
	
	protected  String addProtocolPrefix()
	{
		String protocolPrefix = "#"+ this.getClass().getAnnotation(ProtocolIdentifier.class).protocolId()+";";
		return protocolPrefix; 
	}
		
	protected abstract String parseToStringMessageWithTypeAndString(MessageWithType<String> messageToPars); 
	
	protected abstract String parseToStringUserList(MessageWithType<List<UserInfo>> messageToParse);
	
	protected abstract String parseToStringMessageUserToUser(MessageWithType<MessageFromUserToUserDataContainer> messageToParse);	
}
