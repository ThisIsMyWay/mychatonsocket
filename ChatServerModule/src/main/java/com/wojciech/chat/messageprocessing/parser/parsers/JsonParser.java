package com.wojciech.chat.messageprocessing.parser.parsers;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import com.wojciech.chat.messageprocessing.MessageType;
import com.wojciech.chat.messageprocessing.dataobjects.MessageFromUserToUserDataContainer;
import com.wojciech.chat.messageprocessing.dataobjects.UserInfo;
import com.wojciech.chat.messageprocessing.parser.MessageWithType;

@ProtocolIdentifier(protocolId="json")
public class JsonParser extends ParserAbstract 
{
	@Override
	protected MessageType retrieveMessageTypeFromInput(String inputString) 
	{
		JsonObject jsonObject = getJsonObjectFromString(inputString);
		return MessageType.getTypeByString(jsonObject.getString("type"));
	}

	protected String removePrefixFromMsg(String inputString) 
	{
		return inputString.substring(inputString.indexOf(";")+1);		 
	}

	@Override
	protected List<UserInfo> retrieveListOfUsersFromInput(String inputString) 
	{
		List<UserInfo> listOfUsersToFill = new ArrayList<>();
		JsonArray arrayOfJsonObjects = getJsonObjectFromString(inputString).getJsonArray("data");
		for (int i = 0; i < arrayOfJsonObjects.size(); i++)
		{
			listOfUsersToFill.add(getUserInfoObjectFromJsonObject(arrayOfJsonObjects.getJsonObject(i)));		
		}		
		return listOfUsersToFill;
	}

	@Override
	protected MessageFromUserToUserDataContainer retrieveMessageFromUserToUserObjectFromInput(String string) 
	{
		JsonObject jsonObject = getJsonObjectFromString(string).getJsonObject("data");
		
		return new MessageFromUserToUserDataContainer(getUserInfoObjectFromJsonObject(jsonObject.getJsonObject("fromUser")),
								getUserInfoObjectFromJsonObject(jsonObject.getJsonObject("toUser")), 
							    Long.valueOf(jsonObject.getString("timeOfMsg")),
													  jsonObject.getString("message"));
	}
	
	private UserInfo getUserInfoObjectFromJsonObject(JsonObject jsonObject)
	{
		return new UserInfo(jsonObject.getString("userId"), jsonObject.getString("userName"));
	}
	
	@Override
	protected String retrieveUserName(String string) 
	{
		JsonObject jsonObject = getJsonObjectFromString(string).getJsonObject("data");		
		return jsonObject.getString("userName");		
	}


	private JsonObject getJsonObjectFromString(String input)
	{
		return Json.createReader(new StringReader(input)).readObject();
	}

	@Override
	protected String parseToStringUserList(MessageWithType<List<UserInfo>> messageToParse) 
	{
		JsonObjectBuilder rootJsonObject = Json.createObjectBuilder();
		
		JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
		for (UserInfo userInfo :   messageToParse.getDataObject())
		{
			JsonObjectBuilder userObject = Json.createObjectBuilder();
			userObject.add("userId", userInfo.getUserId());
			userObject.add("userName", userInfo.getUserName());
			arrayBuilder.add(userObject);
		}
		rootJsonObject.add("data", arrayBuilder);
		rootJsonObject.add("type", messageToParse.getMessageType().toString());
		
		return rootJsonObject.build().toString();
	}

	@Override
	protected String parseToStringMessageUserToUser(MessageWithType<MessageFromUserToUserDataContainer> messageToParse) 
	{
		JsonObjectBuilder rootJsonObject = Json.createObjectBuilder();		
		rootJsonObject.add("type", messageToParse.getMessageType().toString());
		
		JsonObjectBuilder dataObject = Json.createObjectBuilder();
		dataObject.add("fromUser",  parseToJsonUserInfoObject(messageToParse.getDataObject().getFromUser()));
		dataObject.add("toUser", parseToJsonUserInfoObject(messageToParse.getDataObject().getToUser()));
		dataObject.add("timeOfMsg", Long.toString(messageToParse.getDataObject().getTimestamp()));
		dataObject.add("message", messageToParse.getDataObject().getMessage());
		rootJsonObject.add("data", dataObject);		
		return rootJsonObject.build().toString();
	}

	@Override
	protected String parseToStringMessageWithTypeAndString(MessageWithType<String> messageToParse) {
		JsonObjectBuilder rootJsonObject = Json.createObjectBuilder();		
		rootJsonObject.add("type", messageToParse.getMessageType().toString());
		JsonObjectBuilder dataObject = Json.createObjectBuilder();

		dataObject.add("userName", messageToParse.getDataObject().toString());
		rootJsonObject.add("data", dataObject);
		return rootJsonObject.build().toString();
	}
	
	private JsonObjectBuilder parseToJsonUserInfoObject(UserInfo userInfo)
	{
		JsonObjectBuilder userInfoJsonObject = Json.createObjectBuilder();
		userInfoJsonObject.add("userId", userInfo.getUserId());
		userInfoJsonObject.add("userName", userInfo.getUserName());
		return userInfoJsonObject;
	}
}
