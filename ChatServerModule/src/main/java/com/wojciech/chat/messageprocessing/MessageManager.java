package com.wojciech.chat.messageprocessing;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import com.wojciech.chat.messageprocessing.dataobjects.MessageFromUserToUserDataContainer;
import com.wojciech.chat.messageprocessing.dataobjects.UserInfo;
import com.wojciech.chat.messageprocessing.parser.MessageWithType;
import com.wojciech.chat.server.ChatUserTask;
import com.wojciech.chat.server.EndConnection;

//here would be executed messages getted from clients 
public class MessageManager 
{	
	private BlockingQueue<ChatUserTask> listOfThreads ;
	
	public MessageManager(BlockingQueue<ChatUserTask> listOfThreads) 
	{
		super();
		this.listOfThreads = listOfThreads;	
	}	
	
	public void putMessageToProcced(ChatUserTask chatUserTask, MessageWithType<?> messageWithType) throws EndConnection
	{
		procceedMessage(chatUserTask, messageWithType);
	}
	
	private void procceedMessage(ChatUserTask chatUserTask, MessageWithType<?> parsedMessage) throws EndConnection
	{
		
		switch (parsedMessage.getMessageType())
		{
		case Init:
			setInitialDataForThread(chatUserTask, parsedMessage);
			sendBackInformationAboutIdOfUser(chatUserTask);
			sendUpdatedListOfUsersToAllClients();
			break;
		case DeliveryReport:
			break;
		case GetUsers:
			getUsersExceptMe(chatUserTask);
			break;
		case Message:
			sendMessage(parsedMessage);
			break;
		case EndConnection:
			chatUserTask.sendMessage(new MessageWithType<String>(MessageType.EndConnection, ""));
			throw new EndConnection();		
		default:
			break;
		}		
	}
	
	private void setInitialDataForThread(ChatUserTask chatUserTask, MessageWithType<?> parsedMessage) 
	{
		chatUserTask.setUserName(parsedMessage.getDataObject().toString());			
	}
	
	private void sendBackInformationAboutIdOfUser(ChatUserTask chatUserTask) 
	{
		chatUserTask.sendMessage(new MessageWithType<String>(MessageType.Init, chatUserTask.getUserId()));
	}

	public void sendUpdatedListOfUsersToAllClients() 
	{
		for (ChatUserTask chatUserTask : listOfThreads) 
		{			
			chatUserTask.sendMessage(new MessageWithType<List<UserInfo>>(MessageType.GetUsers, getUsersExceptMe(chatUserTask)));
		}
	}

	private List<UserInfo> getUsersExceptMe(ChatUserTask myTask) 
	{	
		List<UserInfo> listOfUsers = new ArrayList<>();
		for (ChatUserTask taskToCheck : listOfThreads) 
		{
			if (taskToCheck != myTask)
			{
				listOfUsers.add(new UserInfo(taskToCheck.getUserId(), taskToCheck.getUserName()));
			}
		}
		return listOfUsers;
	}

	private void sendMessage(MessageWithType<?> parsedMessage) 
	{
		MessageFromUserToUserDataContainer mFrom = (MessageFromUserToUserDataContainer) parsedMessage.getDataObject();
		ChatUserTask chatUserTask = getClientTaskById(mFrom.getToUser().getUserId());
		chatUserTask.sendMessage(parsedMessage);
	}
	
	private ChatUserTask getClientTaskById(String userId)
	{
		for (ChatUserTask withClientInformationExchangerThread : listOfThreads) 
		{
			if (userId.equals(withClientInformationExchangerThread.getUserId()))
			{
				return withClientInformationExchangerThread;
			}
		}
		return null;
	}
	

}
