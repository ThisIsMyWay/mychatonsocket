package com.wojciech.chat.messageprocessing.parser;

import com.wojciech.chat.messageprocessing.MessageType;

public class MessageWithType<T> 
{
	
	private MessageType messageType;
	private T dataObject;
	
	public MessageWithType(MessageType messageType, T dataObject) 
	{
		super();
		this.messageType = messageType;
		this.dataObject = dataObject;
		
	}
	
	
	public MessageType getMessageType() 
	{
		return messageType;
	}
	
	public T getDataObject() 
	{
		return dataObject;
	}
	
	

}
