package com.wojciech.chat.messageprocessing;


public enum MessageType 
{	
	Init, GetUsers, Message, DeliveryReport, EndConnection;	
	
	
	public static MessageType getTypeByString(String str)
	{
		return MessageType.valueOf(str);
	}
	
}
