package com.wojciech.chat.messageprocessing.dataobjects;

public class UserInfo 
{
	String userId;
	String userName;

	public UserInfo() {
		super();
	}

	public UserInfo(String userId, String userName) {
		super();
		this.userId = userId;
		this.userName = userName;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserId() {
		return userId;
	}
	
	public String getUserName()
	{
		return userName;
	}

	@Override
	public String toString() 
	{
		return this.userName;
	}
	
	
	
}
