package com.wojciech.chat.messageprocessing.dataobjects;

import java.text.SimpleDateFormat;

public class MessageFromUserToUserDataContainer 
{
	UserInfo fromUser;
	UserInfo toUser;
	long timestamp;
	String message;
	
	
	public MessageFromUserToUserDataContainer()
	{
		
	}
	
	
	public MessageFromUserToUserDataContainer(UserInfo fromUser, UserInfo toUser, long timestamp, String message) {
		super();
		this.fromUser = fromUser;
		this.toUser = toUser;
		this.timestamp = timestamp;
		this.message = message;
	}

	public UserInfo getFromUser() {
		return fromUser;
	}

	public UserInfo getToUser() {
		return toUser;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public String getMessage() {
		return message;
	}


	@Override
	public String toString() 
	{
		return 	fromUser.getUserName() + " " + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(timestamp) + "\n" + message; 
	}
	
	
}
