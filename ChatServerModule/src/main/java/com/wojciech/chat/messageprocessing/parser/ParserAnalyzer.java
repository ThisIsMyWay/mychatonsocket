package com.wojciech.chat.messageprocessing.parser;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Random;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import com.wojciech.chat.messageprocessing.parser.parsers.ParserAbstract;
import com.wojciech.chat.messageprocessing.parser.parsers.ProtocolIdentifier;


public class ParserAnalyzer {

	
	public static MessageWithType<?> retrieveMessageFromInput(String msgText)
	{	
		ParserAbstract parser = getApropriateParserDependentOnMessage(msgText);
		return parser.retrieveParametrizedMessageFromInput(msgText);		
	}

	protected static ParserAbstract getApropriateParserDependentOnMessage(String msgText) {
		String protocolIdentifier = getProtocolId(msgText);
		return 	getParserBasedOnIdentifier(protocolIdentifier);
	}

	protected static String getProtocolId(String msgText) 
	{	    
		String protocolId =  msgText;
		protocolId  = protocolId.substring(protocolId.indexOf("#")+1);
		protocolId  = protocolId.substring(0, protocolId.indexOf(";"));
		return protocolId;
	}
	
	protected static ParserAbstract getParserBasedOnIdentifier(String protocolId) 
	{
		List<ParserAbstract> listOfParser = getAllAvailableParsers();
		for(ParserAbstract parserToCheck : listOfParser)
		{
			ProtocolIdentifier annotiationFromParser = parserToCheck.getClass().getAnnotation(ProtocolIdentifier.class);
			 if (checkIfClassAnnotationMatchToSearchedProtocol(annotiationFromParser, protocolId) ) 
			 {
				 return parserToCheck;
			 }
		}
		return null; 
	}
	
	protected static boolean checkIfClassAnnotationMatchToSearchedProtocol(ProtocolIdentifier annotationFromClass, String protocolId)
	{
		return annotationFromClass.protocolId().equals(protocolId);
	}

	public static String parseMessageObjectToAppropriateInput(MessageWithType<?> parseMessage) 
	{	
		ParserAbstract choosenParser = rendomizeProtocolByWhichWeWillBeSendingData();
		return choosenParser.parseMessageToString(parseMessage);
	}
	
	private static ParserAbstract rendomizeProtocolByWhichWeWillBeSendingData()
	{
		List<ParserAbstract> listOfParsers = getAllAvailableParsers();
		if (listOfParsers.size() == 1)
		{
			return listOfParsers.get(0);
		}
		
		Random randomGenerator = new Random();
        int randomizedIndex = randomGenerator.nextInt(listOfParsers.size()-1);
        return listOfParsers.get(++randomizedIndex);
	}
	
	
	
	// TODO jak testowac, podawac manualnie ile parserow powinno byc dostepnych ?
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static List<ParserAbstract>  getAllAvailableParsers()
	{
		List<ParserAbstract> listOfParsers = new ArrayList<>();

		String pathToJar = ClassLoader.getSystemResource("lib/my_connection_lib.jar").getPath();
		try (JarFile jarFile = new JarFile( new File(pathToJar))) 
		{
			Enumeration<JarEntry> e = jarFile.entries();

			URL[] urls = { new URL("jar:file:" + pathToJar+"!/") };
			URLClassLoader cl = URLClassLoader.newInstance(urls);

			while (e.hasMoreElements()) {
				JarEntry je = e.nextElement();
				if(je.isDirectory() || !je.getName().endsWith(".class")){
					continue;
				}
				String className = je.getName().substring(0,je.getName().length()-6);
				className = className.replace('/', '.');
				Class parserClassToCheck = cl.loadClass(className);
				ProtocolIdentifier annotiationFromCheckedParser = (ProtocolIdentifier) parserClassToCheck.getAnnotation(ProtocolIdentifier.class);
				if (annotiationFromCheckedParser != null)
				{					
					listOfParsers.add((ParserAbstract)(parserClassToCheck).newInstance());
				}
			}		
		} 
		catch (IOException | ClassNotFoundException | InstantiationException | IllegalAccessException | NoClassDefFoundError e1 ) {
	
		}

		return listOfParsers;
	}

}
