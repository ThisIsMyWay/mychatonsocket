package com.wojciech.chat.server;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.SocketException;

import com.wojciech.chat.messageprocessing.MessageManager;
import com.wojciech.chat.messageprocessing.parser.MessageWithType;
import com.wojciech.chat.messageprocessing.parser.ParserAnalyzer;


public class ChatUserTask implements Runnable
{
	BufferedReader reader;
	BufferedWriter writer;
	MessageManager manager;
	private  String userName;
	private String userId;
	private static int userIdCounter = 0;
	

	public ChatUserTask(Socket socket, MessageManager messageManager) {
		super();	
		this.manager = messageManager;
		instatiationOfReaderAndWriterWithGivenSocket(socket);
		generateUserId();
	}

	private void generateUserId() 
	{
		userId = Double.toString(++userIdCounter);		
	}

	
	
	public String getUserId() {
		return userId;
	}

	private void instatiationOfReaderAndWriterWithGivenSocket(Socket socket) 
	{		
		try {
			reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public void run() {
		Server.logger.info(" Connection task with user id(" + userId + ") started on server ");

		String messageFromClient;
		while (true)
		{
			try {
				messageFromClient = reader.readLine();
				Server.logger.info(" From user id(" + userId + ") server got message:\"" + messageFromClient+"\"");
				manager.putMessageToProcced(this, ParserAnalyzer.retrieveMessageFromInput(messageFromClient));
			} 
			catch (EndConnection |SocketException e1)
			{
				break;
			}		
			catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		closeStreams();
		Server.logger.info(" Connection task with user id(" + userId + ") closed on server ");

	}
	
	private void closeStreams() 
	{
		try {
			reader.close();
			writer.close();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}	
	}

	public void sendMessage(MessageWithType<?> parseMessage) 
	{		
		try 
		{
			String input = ParserAnalyzer.parseMessageObjectToAppropriateInput(parseMessage);
			Server.logger.info(" To user id(" + userId + ") server sends message:\"" + input+"\"");
			writer.write(input);
			writer.newLine();
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}



	public String getUserName() {
		return userName;
	}



	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}
