package com.wojciech.chat.server;

import java.net.ServerSocket;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wojciech.chat.messageprocessing.MessageManager;


public class Server 
{
	public  final int numberOfThreads = 10;
	
	public static Logger logger;

    static 
    {
        PropertyConfigurator.configure(ClassLoader.getSystemResourceAsStream("META-INF/log4j.properties"));
        logger = LoggerFactory.getLogger(Server.class);
    }

	
	
	 public Server()
	 {
		
		 ThreadPoolExecutorWithRunningList  executors = new ThreadPoolExecutorWithRunningList(numberOfThreads, numberOfThreads,
                 0L, TimeUnit.MILLISECONDS,
                 new LinkedBlockingQueue<Runnable>());
		 MessageManager messageManager = new MessageManager(executors.getListOfActiveThreads());
		 executors.setMessageManager(messageManager);
		 
		 try(ServerSocket server = new ServerSocket(9191))
		 {
			 logger.info("Server started on port 9191");
			 while(true)
			 {
				 logger.info("Waiting for new thread");
				 executors.execute(new ChatUserTask(server.accept(), messageManager));

			 }
		 }
		 catch (Exception  e) 
		 {
	            e.printStackTrace();
		 }
		 finally 
		 {
			 executors.shutdown();
		 }	
	 }

	 public static void main(String[] args) 
	 {
		 new Server();
	 }


	 
	 
}
