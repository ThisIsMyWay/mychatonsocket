package com.wojciech.chat.server;


import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.wojciech.chat.messageprocessing.MessageManager;

public class ThreadPoolExecutorWithRunningList extends ThreadPoolExecutor 
{

	private BlockingQueue<ChatUserTask> listOfActiveThreads ;
	private MessageManager messageManager = null;
	
	public ThreadPoolExecutorWithRunningList(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit,
			BlockingQueue<Runnable> workQueue) 
	{
		super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
		listOfActiveThreads = new ArrayBlockingQueue<>(corePoolSize);
	}

	
	@Override
	public void execute(Runnable command) {
		super.execute(command);
		listOfActiveThreads.add((ChatUserTask)command);
		
	}

	@Override
	protected void afterExecute(Runnable r, Throwable t) {
		super.afterExecute(r, t);
		listOfActiveThreads.remove((ChatUserTask) r);
		messageManager.sendUpdatedListOfUsersToAllClients();
		
	}

	public void setMessageManager(MessageManager messageManager) {
		this.messageManager = messageManager;
	}


	public BlockingQueue<ChatUserTask> getListOfActiveThreads() {
		return listOfActiveThreads;
	}
	
	

}
