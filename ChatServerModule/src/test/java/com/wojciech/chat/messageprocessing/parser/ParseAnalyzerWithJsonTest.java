package com.wojciech.chat.messageprocessing.parser;


import java.util.ArrayList;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wojciech.chat.messageprocessing.MessageType;
import com.wojciech.chat.messageprocessing.dataobjects.MessageFromUserToUserDataContainer;
import com.wojciech.chat.messageprocessing.dataobjects.UserInfo;
import com.wojciech.chat.messageprocessing.parser.parsers.JsonParser;

public class ParseAnalyzerWithJsonTest  {

	
	JsonParser jsonParser = null;	
	MessageWithType<String> messageWithType = null;
	MessageWithType<List<UserInfo>> messageWithUserList = null;
	MessageWithType<MessageFromUserToUserDataContainer> messageToUser = null;
	
	@Before 
	public void initialize()
	{
		jsonParser = new JsonParser();
		prepareMessages();
	} 
	
	
	private void prepareMessages() 
	{
		messageWithType = new MessageWithType<String>(MessageType.Init,"user1");
		
		List<UserInfo> listOfUsers = new ArrayList<>();
		listOfUsers.add(new UserInfo("123", "user1"));
		listOfUsers.add(new UserInfo("232", "user2"));
		messageWithUserList = new MessageWithType<List<UserInfo>>(MessageType.GetUsers, listOfUsers);
		
		messageToUser = new MessageWithType<MessageFromUserToUserDataContainer>(MessageType.Message,
																				new MessageFromUserToUserDataContainer(
																						new UserInfo("123", "woj"),
																						new UserInfo("234", "what"), 
																						41324141, "hi!"));
				
		
	}


	String jsonForInitialTest = "#json;{\"type\":\"Init\",\"data\":{\"userName\":\"user1\"}}";

	@Test
	public void parseToStringInitialUserDataTest()
	{
		Assert.assertEquals(jsonForInitialTest, ParserAnalyzer.parseMessageObjectToAppropriateInput(messageWithType));
	}	
	
	@Test
	public void retrieveUserNameTest()
	{
		@SuppressWarnings("unchecked")
		MessageWithType<String> retrievedMessageWithUserName = (MessageWithType<String>) ParserAnalyzer.retrieveMessageFromInput(jsonForInitialTest);
		Assert.assertEquals(messageWithType.getMessageType(), retrievedMessageWithUserName.getMessageType());
		Assert.assertEquals(messageWithType.getDataObject(), retrievedMessageWithUserName.getDataObject());

	}
	
	
	String jsonForUserListTest ="#json;{\"data\":[{\"userId\":\"123\",\"userName\":\"user1\"},{\"userId\":\"232\",\"userName\":\"user2\"}],\"type\":\"GetUsers\"}";
	
	@Test 
	public void parseToStringMessageWithUserList() 
	{
		Assert.assertEquals(jsonForUserListTest, ParserAnalyzer.parseMessageObjectToAppropriateInput(messageWithUserList));
	}
	
	@Test 
	public void retrieveListOfUsersFromInputTest()
	{
		@SuppressWarnings("unchecked")
		MessageWithType<List<UserInfo>> retrievedMessageWithUsers = (MessageWithType<List<UserInfo>>) ParserAnalyzer.retrieveMessageFromInput(jsonForUserListTest);
			
		Assert.assertEquals(messageWithUserList.getMessageType(), retrievedMessageWithUsers.getMessageType());
		
		List<UserInfo> retrievedListOfUsers = retrievedMessageWithUsers.getDataObject();
		List<UserInfo> expectedListOfUsers = messageWithUserList.getDataObject();
		Assert.assertEquals(expectedListOfUsers.size(), retrievedListOfUsers.size());

		Assert.assertEquals(expectedListOfUsers.get(0).getUserId(), retrievedListOfUsers.get(0).getUserId());
		Assert.assertEquals(expectedListOfUsers.get(0).getUserName(), retrievedListOfUsers.get(0).getUserName());
		
		Assert.assertEquals(expectedListOfUsers.get(1).getUserId(), retrievedListOfUsers.get(1).getUserId());
		Assert.assertEquals(expectedListOfUsers.get(1).getUserName(), retrievedListOfUsers.get(1).getUserName());
	}
	
	String jsonForMessageToUserTest ="#json;{\"type\":\"Message\",\"data\":{\"fromUser\":{\"userId\":\"123\",\"userName\":\"woj\"},\"toUser\":{\"userId\":\"234\",\"userName\":\"what\"},\"timeOfMsg\":\"41324141\",\"message\":\"hi!\"}}";
	
	@Test 
	public void parseToStringMessageWithMessageFromUserToUser() 
	{
		Assert.assertEquals(jsonForMessageToUserTest, ParserAnalyzer.parseMessageObjectToAppropriateInput(messageToUser));
	}
	
	@Test 
	public void retrieveMessageFromUserToUserTest()
	{
		@SuppressWarnings("unchecked")
		MessageWithType<MessageFromUserToUserDataContainer> retrievedMessageFromUserToUser = (MessageWithType<MessageFromUserToUserDataContainer>) ParserAnalyzer.retrieveMessageFromInput(jsonForMessageToUserTest);
			
		Assert.assertEquals(messageToUser.getMessageType(), retrievedMessageFromUserToUser.getMessageType());

		MessageFromUserToUserDataContainer retrievedMessageContainer = retrievedMessageFromUserToUser.getDataObject();
		MessageFromUserToUserDataContainer expectedMessageContainer = messageToUser.getDataObject();

		Assert.assertEquals(expectedMessageContainer.getFromUser().getUserId(), retrievedMessageContainer.getFromUser().getUserId());
		Assert.assertEquals(expectedMessageContainer.getFromUser().getUserName(), retrievedMessageContainer.getFromUser().getUserName());
		
		Assert.assertEquals(expectedMessageContainer.getToUser().getUserId(), retrievedMessageContainer.getToUser().getUserId());
		Assert.assertEquals(expectedMessageContainer.getToUser().getUserName(), retrievedMessageContainer.getToUser().getUserName());
		
		Assert.assertEquals(expectedMessageContainer.getTimestamp(), retrievedMessageContainer.getTimestamp());
		
		Assert.assertEquals(expectedMessageContainer.getMessage(), retrievedMessageContainer.getMessage());
	}
	

	


	
}
