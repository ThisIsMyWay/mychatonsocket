package com.wojciech.client;

import java.net.UnknownHostException;
import java.util.List;

import com.wojciech.chat.messageprocessing.MessageType;
import com.wojciech.chat.messageprocessing.dataobjects.MessageFromUserToUserDataContainer;
import com.wojciech.chat.messageprocessing.dataobjects.UserInfo;
import com.wojciech.chat.messageprocessing.parser.MessageWithType;
import com.wojciech.chat.server.EndConnection;

public class ClientMessageManager 
{
	private ChatWindowController chatWindowController = null;
	private Client client = null;

	public ClientMessageManager(ChatWindowController chatWindowController) throws UnknownHostException
	{
		super();
		this.chatWindowController = chatWindowController;
		this.client = new Client(this);
		new Thread(client).start();
	}
	
	public void sendMessage(MessageFromUserToUserDataContainer messageFromUserToUserDataContainer)
	{
		client.sendMessage(new MessageWithType<MessageFromUserToUserDataContainer>(MessageType.Message, messageFromUserToUserDataContainer));
	}
		
	@SuppressWarnings("unchecked")
	public void putMessageToProcced(MessageWithType<?> retrieveMessageFromInput) throws EndConnection
	{	
		switch (retrieveMessageFromInput.getMessageType()) {
		case Message:
			chatWindowController.updateDataWithNewMessage((MessageFromUserToUserDataContainer)retrieveMessageFromInput.getDataObject());
			break;
		case Init:
			chatWindowController.setUserIdAssignedByServer((String)retrieveMessageFromInput.getDataObject());
			break;
		case GetUsers:
			chatWindowController.updateListOfAvailableUsers((List<UserInfo>)retrieveMessageFromInput.getDataObject());
			break;
		case EndConnection:
			throw new EndConnection();
		default:
		
			break;
		}
	}

	public void sendInitialData(String ourUserName) {
		client.sendMessage(new MessageWithType<String>(MessageType.Init, ourUserName));		
	}
	
	public void closeConnectionWithServer()
	{
		client.closeConnection();
	}

}
