package com.wojciech.client;

import java.net.UnknownHostException;
import java.util.List;
import java.util.Optional;
import com.wojciech.chat.messageprocessing.dataobjects.MessageFromUserToUserDataContainer;
import com.wojciech.chat.messageprocessing.dataobjects.UserInfo;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.stage.Stage;


public class ChatWindowController 
{
	
	private UserInfo myUser = new UserInfo();
	
	@FXML 
	private ListView<UserInfo> availableUserList; 
	
	private ObservableList<UserInfo> userListData = FXCollections.observableArrayList();
	
	
	@FXML 
	private ListView<MessageFromUserToUserDataContainer> messagesShowerList;
		
	private ObservableList<MessageFromUserToUserDataContainer> messageListData = FXCollections.observableArrayList();
	
	@FXML 
	private TextField inputMessageView;
	
	ClientMessageManager messageManager;
	
	private UserInfo actualUser = null;
	
	@FXML 
	public void initialize()
	{
		showStartupDialog();
		sendInitialConnectionDataToServer();
		addUserListListener();
	}

	private void showStartupDialog() 
	{
		TextInputDialog dialog = new TextInputDialog("gość");
		dialog.getDialogPane().lookupButton(ButtonType.CANCEL).setVisible(false);
		dialog.setTitle("Okno logowania");
		dialog.setHeaderText("Tutaj wpisz swój login, jeśli będziesz chciał ominąć to okno \n login wybierzemy za Ciebie :D");
		dialog.setContentText("Podaj login:");

		Optional<String> result = dialog.showAndWait();
		result.ifPresent((nickName) -> {
		    if (nickName.trim().isEmpty())
		    {
		    	myUser.setUserName("gość");
		    }
		    else 
		    {
			    myUser.setUserName(nickName);
		    }
		});		
		
	}

	
	private void sendInitialConnectionDataToServer() 
	{
		try {
			messageManager = new ClientMessageManager(this);
		} catch (UnknownHostException e) {
			// show some message that couldn't connect
			e.printStackTrace();
		}
		messageManager.sendInitialData(myUser.getUserName());		
	}


	
	private void addUserListListener() 
	{		
		availableUserList.getSelectionModel().selectedItemProperty().addListener((p, oldValue, newValue) -> 		
		{
			if (newValue == null)
			{
				inputMessageView.setDisable(true);
			}
			inputMessageView.setDisable(false);
			actualUser = newValue;
			
			updateMessageView();			
		});
	}


	private void updateMessageView() {
		
		if (actualUser == null)
		{
			return;
		}
		
		messagesShowerList.setItems(messageListData.filtered( msg -> 
		{
			if ((msg.getFromUser().getUserId().equals(myUser.getUserId()) && 
			    msg.getToUser().getUserId().equals(actualUser.getUserId())) ||
				(msg.getFromUser().getUserId().equals(actualUser.getUserId()) && 
			    msg.getToUser().getUserId().equals(myUser.getUserId())))			    
			{
				return true;
			}
			return false;
		}));
		

	}

	@FXML 
	public void sendMessage()
	{
		MessageFromUserToUserDataContainer messageToSend = new MessageFromUserToUserDataContainer(this.myUser, this.actualUser ,System.currentTimeMillis()  , inputMessageView.getText());
		messageManager.sendMessage(messageToSend);
		updateDataWithNewMessage(messageToSend);
		System.out.println("Sended" + inputMessageView.getText());
		inputMessageView.clear();
		messagesShowerList.scrollTo(messageListData.size() -1);
	}
	
	public void updateDataWithNewMessage(MessageFromUserToUserDataContainer messageFromUserToUserDataContainer)
	{
		Platform.runLater(new Runnable() {
			@Override 
			public void run() 
			{		
				messageListData.add(messageFromUserToUserDataContainer);
				updateMessageView();	
			}});
	}


	public void setUserIdAssignedByServer(String userId) 
	{
		this.myUser.setUserId(userId);
	}


	public void updateListOfAvailableUsers(List<UserInfo> dataObject) 
	{	
		UserInfo ourUserFromNewList = null;	
		if (actualUser != null)
		{
			ourUserFromNewList = getChoosenUserFromGettedList(dataObject);
			if (ourUserFromNewList == null)
			{
				inputMessageView.setDisable(false);			
				showErrorMessage("użytkownik z którym rozmawiałeś zwiał");				
			}			
		}
		
		Platform.runLater(new Runnable() {
			@Override 
			public void run() 
			{				
				userListData.clear();
				userListData.addAll(dataObject);
				availableUserList.setItems(userListData);	
			}});	
		
		if (actualUser != null)
			availableUserList.getSelectionModel().select(userListData.indexOf(ourUserFromNewList));	
	}	
	
	
	
	public UserInfo getChoosenUserFromGettedList(List<UserInfo> dataObject)
	{
		for (UserInfo item : dataObject) 
		{
			if (item.getUserId().equals(actualUser.getUserId()))
				return item;
		}
		return null;
	}
	
	public void showErrorMessage(String message)
	{
		Platform.runLater(new Runnable() {
			@Override 
			public void run() 
			{
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Uppps");
				alert.setHeaderText("Coś poszło nie po Twojej myśli");
				alert.setContentText(message);
				alert.showAndWait();
			}});
	}

	Stage primaryStage = null;
	
	public void setPreStage(Stage primaryStage) {
		this.primaryStage = primaryStage;
		this.primaryStage.setOnHidden(p -> 
		{
			messageManager.closeConnectionWithServer();
		});
	}

	
}
