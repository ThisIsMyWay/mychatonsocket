package com.wojciech.client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import static com.wojciech.chat.messageprocessing.MessageType.*;
import com.wojciech.chat.messageprocessing.parser.MessageWithType;
import com.wojciech.chat.messageprocessing.parser.ParserAnalyzer;
import com.wojciech.chat.server.EndConnection;



public class Client implements Runnable {

    private InetSocketAddress address;
	BufferedReader reader;
	BufferedWriter writer;
	ClientMessageManager manager;
		
	public Client(ClientMessageManager manager) throws UnknownHostException {
		super();
		this.manager = manager;
		this.address = new InetSocketAddress(InetAddress.getLocalHost(), 9191);		
		Socket socket;
		try {
			socket = new Socket(InetAddress.getByName(address.getHostName()), this.address.getPort());
			instatiationOfReaderAndWriterWithGivenSocket(socket);
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}	
	}

	private void instatiationOfReaderAndWriterWithGivenSocket(Socket socket) 
	{		
		try {
			reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public void run() {
		String messageFromClient;
		while (true)
		{
			try 
			{
				if (reader.ready())
				{
					messageFromClient = reader.readLine();
					System.out.println(messageFromClient);
					manager.putMessageToProcced(ParserAnalyzer.retrieveMessageFromInput(messageFromClient));
				}		
			}		
			catch (EndConnection ex)
			{
				break;
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}			
		}
		closeStreams();
	}
	
	public void closeConnection()
	{
		this.sendMessage(new MessageWithType<String>(EndConnection, ""));
	}
	
	private void closeStreams() 
	{
		try {
			reader.close();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}

	public void sendMessage(MessageWithType<?> parseMessage) 
	{		
		try 
		{
			writer.write(ParserAnalyzer.parseMessageObjectToAppropriateInput(parseMessage));			
			writer.newLine();
			writer.flush();
		} 
		catch (IOException e) 
		{			
			e.printStackTrace();
		}		
	}
}
