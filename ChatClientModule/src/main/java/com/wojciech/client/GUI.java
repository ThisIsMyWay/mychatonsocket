package com.wojciech.client;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class GUI extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		FXMLLoader loader = new FXMLLoader(ClassLoader.getSystemResource("fxml/ChatMainScreen.fxml"));
		Parent parent = loader.load();
		ChatWindowController controller = loader.getController();
        controller.setPreStage(primaryStage);
		Scene scene = new Scene(parent, 500, 700);

		primaryStage.setScene(scene);
		primaryStage.setTitle("MyChatClient");
		primaryStage.setMinWidth(500);

		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}

}
